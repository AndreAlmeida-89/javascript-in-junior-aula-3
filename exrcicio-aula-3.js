/*
    Criar uma função que, dado um número indefinido de arrays,
    faça a soma de seus valores
    Ex: [1,2,3] [1,2,2] [1,1] => 13
        [1,1] [2, 20] => 24
*/

function somar(...arr){
    let soma = 0;
    arr.forEach(e => {
        e.forEach(n => {
            soma += n;
        });   
    });
    return soma;
}

console.log(somar([1,2,3],[4,5,6]));

/*
    Criar uma função que dado um número n e um array, retorne
    um novo array com os valores do array anterior * n
    Ex: (2, [1,3,6,10]) => [2,6,12,20]
        (3, [7,9,11,-2]) => [21, 27, 33, -6]
*/

function multArr (n, arr){
    let noVaArr = arr.map (e => e * n);
    return noVaArr;
}
console.log(multArr(2,[1,3,6,10]));

/*
    Crie uma função que dado uma string A e um array de strings,
    retorne um array novo com apenas as strings do array que são
    compostas exclusivamente por caracteres da string A

    Ex: ("ab", ["abc", "ba", "ab", "bb", "kb"]) => ["ba", "ab", "bb"]
        ("pkt", ["pkt", "pp", "pata", "po", "kkkkkkk"]) => ["pkt", pp, kkkkkkk]
*/

function filtraString(str, arr){
    let novaArr = arr.filter(e => {
        let ok = true;
        for (letra of e){
            if (!str.includes(letra)){
                ok = false;
            }
        }
        if (ok){
            return e;
        }
    })
    return novaArr;
}

console.log(filtraString("ab", ["abc", "ba", "ab", "bb", "kb"]))



/*
    Criar uma função que dado n arrays, retorne um novo array que possua
    apenas os valores que existem em todos os n arrays
    Ex: [1, 2, 3] [3, 3, 7] [9, 111, 3] => [3]
        [120, 120, 110, 2] [110, 2, 130] => [110, 2]
*/

function filtraValorComum(...arr){
    for(let i = 0; i < arr.length - 1; i++){
        return arr[i].filter(value => -1 !== arr[i+1].indexOf(value));
    }
}


console.log(filtraValorComum([120, 120, 110, 2],[110, 2, 130]));


/*
    Crie uma função que dado n arrays, retorne apenas os que tenham a
    soma de seus elementos par
    Ex: [1, 1, 3] [1, 2, 2, 2, 3] [2] => [1, 2, 2, 2, 3] [2]
        [2,2,2,1] [3, 2, 1] => [3,2,1] 
*/

function somaPar(...arr){
    let res = [];
    arr.forEach(e => {
        let soma = 0;
        e.forEach(n => {
            soma += n;
        });
        if (soma % 2 === 0){
            res.push(e);
        }
    });
    return res;
}

console.log(somaPar([1, 1, 3], [1, 2, 2, 2, 3], [2]));